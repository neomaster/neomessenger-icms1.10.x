<?php

    if (!defined('VALID_CMS')) { die('ACCESS DENIED'); }

    $_LANG['PNM_TIME_UPDATE']   = 'таймаут обновления чата (сек)';
    $_LANG['PNM_SEND_TYPE']     = 'Отправка сообщений по:<br /> 1 - "Enter",<br /> 0 - "Ctrl+Enter"';
    $_LANG['PNM_CLOSE_OVERLAY'] = 'Закрыть окно кликнув на задний фон:<br /> 1 - "Да",<br /> 0 - "Нет"';
    $_LANG['PNM_USERS_DEBUG']   = 'Показывать мессенджер пользователям только с этим(и) ид (через запятую), или всем (оставте поле пустым)';